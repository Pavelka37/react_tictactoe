import React from 'react';
import './App.css';
import GameCreate from './components/GameCreate';
import ShowAllGames from './components/ShowAllGames';
import Play from './components/Play';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';

  class App extends React.Component{
    constructor(props) {
      super(props);
      this.state={
        size:0,win:0
      }
    }

    Home = () =>{
      return(
        <div>
          <h1>Home Page</h1>
        </div>
      )
    }

    render(){
      return (
     
        <Router>
    
        <div className = "menu">

        <Route path = "/" component = {this.Home}/>
        <div className = "nav">
          <Link to = "/">Domu</Link>
          <Link to = "gamecreate">Vytvorit</Link>
          <Link to = "showallgames">Zobrazit vsechny</Link>
          <Link to = "play">Hrat</Link>
        </div>
        <Route path = "/gamecreate" component={GameCreate}/>
        <Route path = "/showallgames" component={ShowAllGames}/>
        <Route path = "/play" component={Play}/>

        </div>
        </Router>
      
    );
  }

}
export default App;
