console.clear();

function findValueOfObj(arr,x,y){
    const object = arr.find(obj =>{return obj.x === x && obj.y === y});
    return object.value;
}

function setValueOfObj(arr,x,y,val){
    const obj = arr.find(obj =>{return obj.x === x && obj.y === y});
    obj.value = val;
    return arr;
}

function createArray(size) {
    let arr = [];
    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            arr.push({
                x:i,
                y:j,
                value:"-"
            })
        }
    }
    return arr;
}

function controllWin(arr, win) {
    let ret = 0;
    let counter;
    let size = Math.sqrt(arr.length);
    let sign;

    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            sign = findValueOfObj(arr,i,j);
            if(sign == "X" || sign == "O"){
                counter = 0;
                if(i < size - win){
                    for(let p = 0; p < win ; p++){
                        if(findValueOfObj(arr,i+p,j) == sign){
                            counter++;
                        }
                    }
                    if(counter == win){
                        if(sign == "X"){
                            ret = 1;
                        }else{
                            ret = 2;
                        }
                    }
                }
                counter = 0;
                if(j < size - win){
                    for(let p = 0; p < win ; p++){
                        if(findValueOfObj(arr,i,j+p) == sign){
                            counter++;
                        }
                    }
                    if(counter == win){
                        if(sign == "X"){
                            ret = 1;
                        }else{
                            ret = 2;
                        }
                    }
                }
                counter = 0;
                if(i < size - win && j < size - win){
                    for(let p = 0; p < win ; p++){
                        if(findValueOfObj(arr,i+p,j+p) == sign){
                            counter++;
                        }
                    }
                    if(counter == win){
                        if(sign == "X"){
                            ret = 1;
                        }else{
                            ret = 2;
                        }
                    }
                }
                counter = 0;
                if(i < size - win && j >= (win-1)){
                    for(let p = 0; p < win ; p++){
                        if(findValueOfObj(arr,i+p,j-p) == sign){
                            counter++;
                        }
                    }
                    if(counter == win){
                        if(sign == "X"){
                            ret = 1;
                        }else{
                            ret = 2;
                        }
                    }
                }
            }
        }
    }
    return ret;
}

function game(player, arr, x, y) {
    let ret;
    size = Math.sqrt(arr.length);
    if ((x >= 0) && (x < size) && (y >= 0) && (y < size) && (findValueOfObj(arr,x,y) === '-')) {
        if (player === "X") {
            arr = setValueOfObj(arr,x,y,"X");
            player = "O";
        } else {
            arr = setValueOfObj(arr,x,y,"O");
            player = "X";
        }
    }
    ret = [arr,player];
    return ret;
}

module.exports = {
    game: game,
    createArray: createArray,
    controllWin: controllWin
}









