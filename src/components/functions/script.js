
const express = require('express');
const app = express();
const obj = require('./tictactoe.js');
const bodyParser = require('body-parser');

let listOfGames = [];
let id = 0;

app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  next();
});

app.post("/games", (req, res) => {
  let size = req.body.size;
  let win = req.body.win;
  let arr = obj.createArray(size);
  id++;
  res.send( listOfGames[id] = {
    id,
    arr,
    size,
    win,
    player : "X",
    inGame: true,
    winner: ""
  });


});

app.get("/gamesall", (req, res) => { //Request pro vypsani vsech her
  let resArr = [];
  listOfGames.forEach(e => {
      resArr.push(e.arr);
  });
  res.send(resArr);
})

app.get("/gamesid", (req, res) => { //Request pro vypsani urcite hry
 idgame = req.query.id;
 res.send(listOfGames[idgame].arr);
})

app.post("/games/:id/move", (req, res) => {//Request pro pohyb
  id = req.params.id;
  arr = listOfGames[id].arr;
  win = listOfGames[id].win;
  let retu = [];
  player = listOfGames[id].player;
    

    
  if(listOfGames[id].inGame == true){
    let x = parseInt(req.body.y);
    let y = parseInt(req.body.x);
    let ret = obj.game(player, arr, x, y);
    let arrNew = ret[0];
    listOfGames[id].player = ret[1];
    retu.push(arrNew);
    if (obj.controllWin(arr, win) === 1) {
      listOfGames[id].inGame = false;
      listOfGames[id].winner = "X";
      retu.push("Winner X");
    }
    else if(obj.controllWin(arr, win) === 2){
      listOfGames[id].inGame = false;
      listOfGames[id].winner = "O";
      retu.push("Winner O");
    }
  }
  res.send(retu);
});


app.listen(2000, () => {
  console.log("Listening on 2000");
});
