const {ApolloServer, gql} = require('apollo-server');
/*const express = require('express');
const app = express();
const obj = require('./tictactoe.js');
const bodyParser = require('body-parser');*/
const functionsPack = require('./tictactoe');
let listOfGames = [];

const typeDefs = gql`
  
  type Game{
    id:ID!,
    size:Int!,
    win:Int!,
    arr:[Row]!,
    player:String!,
    inGame:Boolean
  }

  type Row{
      data:[Data]!
  }

  type Data{
      value:String!
  }

  type Query{
    games:[Game],
    game(id:ID!):Game
    move(id:ID!,x:Int!,y:Int!):Game
  }

  type Mutation{
    createGame(size:Int!,win:Int!):Game
  }
`;

const resolvers = {
  Query:{
    games:() => {
      return listOfGames;
    },
    game:(parent,args) =>{
      id = args.id;
      return listOfGames[id];
    },
    move:(parent,args)=>{
      id = args.id;
      arr = listOfGames[id].arr;
      win = listOfGames[id].win;
      let retu = [];
      player = listOfGames[id].player;
        
        
        
      if(listOfGames[id].inGame == true){
        let x = args.y;
        let y = args.x;
      
        let ret = functionsPack.game(player, arr, x, y);
        let arrNew = ret[0];
        listOfGames[id].player = ret[1];
        retu.push(arrNew);
        if (functionsPack.controllWin(arr, win) === 1) {
          listOfGames[id].inGame = false;
          listOfGames[id].winner = "X";
          retu.push("Winner X")
        }
        else if(functionsPack.controllWin(arr, win) === 2){
          listOfGames[id].inGame = false;
          listOfGames[id].winner = "O";
          retu.push("Winner O");
        }
      }
      return retu;
    }
  },
  Mutation:{
    createGame:(parent,args) =>{
      const {size,win} = args;
      const id = listOfGames.length;
      const arr = functionsPack.createArray(size);
      const player = "X";
      const inGame = true;
      const newGame = {id,size,win,player,inGame,arr};
      listOfGames.push(newGame);
      return newGame;
    }
  }
}

const server = new ApolloServer({
  typeDefs,resolvers
});

server.listen({port:4000}).then((res)=>{
  console.log(`Server running on url ${res.url}`);
})