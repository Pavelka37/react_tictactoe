import React, { Component } from 'react';
import axios from 'axios';
import Game from './Game';
class ShowAllGames extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            data: [],
            clicked: false
        }
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick = () => {
        this.setState({ clicked: true });
        let arr = [];
        axios.get('http://localhost:2000/gamesall').then(res => {
            if (res.data.length === 0) {
                this.setState({ message: "Nevytvořili jste zatím žádnou hru" });
            }
            else {
                this.setState({data: res.data});
            }
        });
    }

    render() {
        if (this.state.clicked === false) {
            return (
                <React.Fragment>
                    <h1>Vsechny hry</h1>
                    <button className="UI" onClick={this.handleClick}>Zobrazit vsechny hry</button>
                </React.Fragment>
            );
        }
        else {
            return (
                <React.Fragment>
                    <h1>Vsechny hry</h1>
                    <div className = "games">{this.state.data.map((game) => <Game game = {game} id = {game.id}/>)}</div>
                    <p>{this.state.message}</p>
                </React.Fragment>
            );
        }
    }
}
export default ShowAllGames;