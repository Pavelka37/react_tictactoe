import React, { Component } from 'react';
class Square extends Component {
    constructor(props){
        super(props);
    }

    click = () =>{
        if(this.props.isActive){
        this.props.handleClick(this.props.squareIndex,this.props.rowIndex);
        }
    }

    render() {
        return (<div className = "square" onClick = {this.click}>{this.props.value.value}</div>)
    }
}

export default Square;
