import React, { Component } from 'react'
import Row from './Row';
class Game extends React.Component{
    constructor(props){
        super(props);
    }

    makeBoard = (data) =>{
        const board = [];
        for(let j = 0; j < Math.sqrt(data.length);j++){
            const rows = [];
            for(let i = 0; i < Math.sqrt(data.length);i++){
                rows.push(data.find(obj=>{return obj.x === i && obj.y === j}));
            }
            board.push(rows);
        }
        return board;
    }

    render(){
        return(
            <div className = "game">{this.makeBoard(this.props.game).map((row,rowIndex) => <Row row={row} rowIndex = {rowIndex} id = {this.props.id} handleClick = {this.handleClick} isActive = {false}/>)}</div>
        )
    }

}
export default Game;