import React, { Component } from 'react';
import axios from 'axios';
import Row from './Row';
class Play extends Component {
    constructor(props){
        super(props);

        this.state = { 
            id:'',
            data:[],
            submit:0,
            message:''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e =>{
        this.setState({[e.target.name]:e.target.value});
    } 

     handleSubmit = e =>{
         e.preventDefault();
         this.setState({submit:1});

         axios.get(`http://localhost:2000/gamesid`,{params:{id:this.state.id}}).then(res => {
            this.setState({data:res.data})

        })
    }

    handleClick = (rowIndex,squareIndex) => {
        axios.post(`http://localhost:2000/games/${this.state.id}/move`,{
            x:squareIndex,
            y:rowIndex
        }).then(res=>{
            if(res.data.length === 1){
                this.setState({data:res.data[0]});
            }else{
                this.setState({data:res.data[0]});
                this.setState({message:res.data[1]});
            }
        })

    }


    makeBoard = (data) =>{
        const board = [];
        for(let j = 0; j < Math.sqrt(data.length);j++){
            const rows = [];
            for(let i = 0; i < Math.sqrt(data.length);i++){
                rows.push(data.find(obj=>{return obj.x === i && obj.y === j}));
            }
            board.push(rows);
        }
        return board;
    }


    render() { 
        if(this.state.submit === 0){
        return ( <React.Fragment>
            <h1>Play</h1>
            <form onSubmit = {this.handleSubmit}>
            <div class = "formItem">
                    <label for = "id">ID Hry, kterou chcete hrat</label>
                    <input name = "id" type = "number" value = {this.state.id} onChange = {this.handleChange}/>
            </div>
            <button type = "submit" class = "UI">Hrat hru</button>
            </form>
        </React.Fragment> );
        }else{
            console.log(this.makeBoard(this.state.data));
            return(
                <React.Fragment>
                <div className = "game">
                {this.makeBoard(this.state.data).map((row,rowIndex) => <Row row={row} rowIndex = {rowIndex} id = {this.state.id} handleClick = {this.handleClick} isActive = {true}/>)}
                </div>
                <div className = "message">
                {this.state.message}
                </div>
                </React.Fragment>
            );
        }
    }
}
 
export default Play;