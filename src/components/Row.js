import React, { Component } from 'react';
import Square from './Square';

class Row extends Component {

    render() { 
        return (  
          <div className = "row">{this.props.row.map((squareValue,squareIndex) => <Square value={squareValue} handleClick = {this.props.handleClick} rowIndex = {this.props.rowIndex} squareIndex = {squareIndex} isActive = {this.props.isActive}/>)}</div>
        );
    }
}
 
export default Row;