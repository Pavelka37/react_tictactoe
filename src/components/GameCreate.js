import React, { Component } from 'react';
import axios from 'axios';

class CreateGame extends Component {
    constructor(props) {
        super(props);
        this.state = {
            size: 0,
            win: 0,
            message:" "
        }
    }

    makeArr = (size) => {
        let arr = [];
        for (let i = 0; i < size; i++) {
            let row = [];
            for (let j = 0; j < size; j++) {
                row.push("-");
            }
            arr.push(row);
        }
        return arr;
    }
    
    handleChange = e =>{
        this.setState({[e.target.name]:e.target.value});
    }

    handleSubmit = (e)=>{
        e.preventDefault();
        axios.post('http://localhost:2000/games', {
            size:this.state.size,
            win:this.state.win
    })
        .then(response => {
            this.setState({message:"Hra byla vytvorena, id hry : " + response.data.id});
        })

        
    }

    render() { 
        return ( 
            <React.Fragment>
                <h1>Vytvořte hru Piškvorek</h1>
                <h2><strong>Zadejte hodnoty pro velikost pole a počtu policek potrebnych k vyhre</strong></h2>
                <h3>Doporucuji <strong>Velikost pole : 8</strong> a <strong>Pocet k vyhre: 3</strong></h3>

                <form onSubmit = {this.handleSubmit}>
                    <div class = "formItem">
                    <label for = "size">Velikost pole</label>
                    <input name = "size" type = "number" value = {this.state.size} onChange = {this.handleChange}/>
                    </div>
                    <br/>
                    <div class = "formItem">
                    <label for = "win">Pocet k vyhre</label>
                    <input name = "win" type = "number" value = {this.state.win} onChange = {this.handleChange}/>
                    </div>
                    <br/>
                    <div class = "formItem">
                    <button type = "submit" class = "UI">Vytvorit hru</button>
                    </div>
                </form>
                <h1 className = "message">{this.state.message}</h1>
            </React.Fragment>
         );
    }
}
 
export default CreateGame;